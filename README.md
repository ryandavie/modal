# Ngx Dynamic Modals

Modals with dynamically injected content. 

Supports: 

- Statically typed components
- Unlimited nested modals
- Custom styling, independant to each modal


# Configuration

1. Add a modal outlet in app.component.html:
    
```
<app-modal></app-modal>
```

2. Register ModalService in providers in app.module.ts: 

```
providers: [ModalService]
```

3. Add components to use inside modals to entryComponents in app.module.ts:

```
entryComponents: [
    CustomModalComponent,
    SecondCustomModalComponent
  ]
```
  
# Usage

Provide ModalService in the component: 

```
constructor(private modalService: ModalService) { }
```

Use ModalService to open a modal with a supplied component type:

```
var componentReference = this.modalService.open<CustomModalComponent>(CustomModalComponent);
```

componentReference is of type CustomModalComponent, so we can get autocomplete on the property and methods. Set properties and subscribe to component outputs:

```
componentReference.propertyOne = 'Test String'; // This content will be updated in the modal

componentReference.onActionCompleted.subscribe(result => {
    // Code called on action completed.
});

```


#   Todo

- Default animations (based on bootstrap)
- Custom styles
- Create module
- Module settings
