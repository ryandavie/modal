import { Injectable, Type } from '@angular/core';
import { ModalComponent } from '../components/modal/modal.component';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  private ready: boolean;
  private modalComponent: ModalComponent;

  constructor() { }

  public open<T>(component: Type<any>, options?: ModalOptions) {
    return this.modalComponent.open<T>(component, options);
  }

  public close(){
    this.modalComponent.close();
  }

  public closeAll(){
    this.modalComponent.closeAll();
  }

  public setOutlet(component: ModalComponent){
    this.modalComponent = component;
  }
}

export class ModalOptions {

}
