import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ModalModule } from 'angular-custom-modal';
import { ModalComponent } from './components/modal/modal.component';
import { ModalService } from './services/modal.service';
import { ContentComponent } from './components/content/content.component';
import { ModalDirective } from './directives/modal.directive';
import { ConsumerComponent } from './components/consumer/consumer.component';
import { SecondContentComponent } from './components/second-content/second-content.component';

@NgModule({
  declarations: [
    AppComponent,
    ModalComponent,
    ContentComponent,
    ModalDirective,
    ConsumerComponent,
    SecondContentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ModalModule
  ],
  providers: [ModalService],
  bootstrap: [AppComponent],
  entryComponents: [
    ContentComponent,
    SecondContentComponent
  ]
})
export class AppModule { }
