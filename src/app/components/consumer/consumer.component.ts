import { Component, OnInit } from '@angular/core';
import { ModalComponent } from '../modal/modal.component';
import { ModalService } from 'src/app/services/modal.service';
import { ContentComponent } from '../content/content.component';

@Component({
  selector: 'app-consumer',
  templateUrl: './consumer.component.html',
  styleUrls: ['./consumer.component.scss']
})
export class ConsumerComponent implements OnInit {

  constructor(private modalService: ModalService) { }

  ngOnInit() {
  }

  openModal() {
    var result = this.modalService.open<ContentComponent>(ContentComponent);
    result.property = 1;
  }
}
