import { Component, OnInit, ViewChild, ComponentFactoryResolver, Type, ComponentRef, ApplicationRef, ViewChildren, QueryList } from '@angular/core';
import { ModalDirective } from 'src/app/directives/modal.directive';
import { ModalOptions, ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  // A list of modal contents hosts in the view
  @ViewChildren(ModalDirective) contentHosts: QueryList<ModalDirective>;

  // An array of modals currently open
  modalInstances: Array<ModalInstance> = new Array<ModalInstance>();

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private modalService: ModalService,
    private appRef: ApplicationRef
  ) { }

  ngOnInit() {
    // Register this component as an outlet for the modal service
    this.modalService.setOutlet(this);
  }

  // Opens a new modal of component type T, with optional modal options
  public open<T>(component: Type<any>, options?: ModalOptions): T {

    // Push a new modal into our modal collection, then call appRef.tick() to refresh the view
    // Need to do this before we generate dynamic content, or there will be no components found to inject modal contents
    let instance = new ModalInstance();
    this.modalInstances.push(instance);
    this.appRef.tick();

    // Generate dynamic content
    let componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
    let viewContainerRef = this.contentHosts.toArray()[this.contentHosts.length - 1].viewContainerRef;
    let componentRef: ComponentRef<any> = viewContainerRef.createComponent(componentFactory);
    instance.componentRef = componentRef;
    // Return statically typed instance of the generated component.
    return (<T>componentRef.instance);
  }

  // Close the most recent modal.
  public close() {
    let modal = this.modalInstances.pop();
    modal.componentRef.destroy();
  }

  // Close all modals
  public closeAll() {
    // Remove all modal instances and destroy contents
    let numberOfModals = this.modalInstances.length;
    for (let i = 0; i < numberOfModals; i++) {
      let modal = this.modalInstances.pop();
      modal.componentRef.destroy();
    }
  }

  // TODO this will be optional with settings
  // Close most recent modal on click exit button or overlay.
  overlayClick(e) {
    if (e.target.id == 'ngx-dynamic-modal-close-btn' || e.target.id == 'ngx-dynamic-modal-overlay') {
      this.close();
    };
  }
}

class ModalInstance {
  constructor(public componentRef?: ComponentRef<any>) { }
}
