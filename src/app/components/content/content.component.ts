import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';
import { SecondContentComponent } from '../second-content/second-content.component';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  property: number = 1;

  constructor(private modalService: ModalService) { }

  ngOnInit() {
  }

  close(){
    this.property++;
  }

  open(){
    this.modalService.open<ContentComponent>(ContentComponent);
  }

  closeAll(){
    this.modalService.closeAll();
  }
}
